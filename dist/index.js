webpackJsonp([0],{

/***/ 120:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Icon = Icon;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Icon(props) {
	return _react2.default.createElement("i", { className: "fas fa-" + props.name });
}

/***/ }),

/***/ 121:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var Config = exports.Config = {
	apiUrl: 'dist/api/',
	layerTemplate: {
		title: "New Map Layer",
		attribution: "",
		url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
	},
	map: {
		mapLat: 48.8,
		mapLon: 132.95,
		zoom: 13
	}
};

/***/ }),

/***/ 198:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _icon = __webpack_require__(120);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = __webpack_require__(41);

var _overlay = __webpack_require__(397);

var _overlay2 = _interopRequireDefault(_overlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ServerError = function (_React$Component) {
	_inherits(ServerError, _React$Component);

	function ServerError(props) {
		_classCallCheck(this, ServerError);

		return _possibleConstructorReturn(this, (ServerError.__proto__ || Object.getPrototypeOf(ServerError)).call(this, props));
	}

	_createClass(ServerError, [{
		key: 'render',
		value: function render() {
			var alert = _react2.default.createElement(
				_reactBootstrap.Alert,
				{ bsStyle: 'danger', className: 'alert-inboxed', onDismiss: this.props.action },
				_react2.default.createElement(
					'h4',
					null,
					'\u041E\u0448\u0438\u0431\u043A\u0430 \u0432\u0437\u0430\u0438\u043C\u043E\u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044F \u0441 \u0441\u0435\u0440\u0432\u0435\u0440\u043E\u043C!'
				),
				_react2.default.createElement(
					'p',
					null,
					'\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u043F\u0440\u043E\u0447\u0438\u0442\u0430\u0442\u044C/\u0441\u043E\u0445\u0440\u0430\u043D\u0438\u0442\u044C \u0434\u0430\u043D\u043D\u044B\u0435...'
				),
				_react2.default.createElement(
					'p',
					null,
					_react2.default.createElement(
						_reactBootstrap.Button,
						{ bsStyle: 'danger', onClick: this.props.action },
						this.props.actionText || 'Ок'
					)
				)
			);

			return _react2.default.createElement(_overlay2.default, { show: true, content: alert });
		}
	}]);

	return ServerError;
}(_react2.default.Component);

exports.default = ServerError;

/***/ }),

/***/ 235:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(12);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _index = __webpack_require__(246);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_reactDom2.default.render(_react2.default.createElement(_index2.default, null), document.querySelector('.map-app'));

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(12);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactAutobind = __webpack_require__(40);

var _reactAutobind2 = _interopRequireDefault(_reactAutobind);

var _reactBootstrap = __webpack_require__(41);

var _layers_list = __webpack_require__(396);

var _layers_list2 = _interopRequireDefault(_layers_list);

var _edit_layer = __webpack_require__(398);

var _edit_layer2 = _interopRequireDefault(_edit_layer);

var _server = __webpack_require__(399);

var _server2 = _interopRequireDefault(_server);

var _config = __webpack_require__(121);

var _map = __webpack_require__(418);

var _map2 = _interopRequireDefault(_map);

var _server_error = __webpack_require__(198);

var _server_error2 = _interopRequireDefault(_server_error);

var _app_error = __webpack_require__(555);

var _app_error2 = _interopRequireDefault(_app_error);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
	_inherits(App, _React$Component);

	function App(props) {
		_classCallCheck(this, App);

		var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

		_this.state = {
			error: '',
			list: false,
			selected: 0,
			edit: '',
			maxId: 0
		};
		(0, _reactAutobind2.default)(_this);
		return _this;
	}

	_createClass(App, [{
		key: 'componentDidCatch',
		value: function componentDidCatch() {
			this.setState({
				error: 'app'
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.reloadList();
		}
	}, {
		key: 'reloadList',
		value: function reloadList() {
			var _this2 = this;

			_server2.default.reloadList().then(function (_ref) {
				var data = _ref.data;

				var maxId = 0;
				data.layers.forEach(function (row) {
					if (row.id > maxId) maxId = row.id;
				});
				_this2.setState({
					maxId: maxId++,
					list: data.layers,
					selected: 0,
					edit: ''
				});
			}).catch(this.displayServerError);
		}
	}, {
		key: 'displayServerError',
		value: function displayServerError() {
			this.setState({
				list: null,
				error: 'server'
			});
		}
	}, {
		key: 'setNewLayerOnMap',
		value: function setNewLayerOnMap(index) {
			this.setState({
				selected: index
			});
		}
	}, {
		key: 'handleLayerAction',
		value: function handleLayerAction(action) {
			var method = 'handleLayerAction' + action;
			if (typeof this[method] !== 'undefined') {
				this[method](action);
			}
		}
	}, {
		key: 'handleLayerActionRemove',
		value: function handleLayerActionRemove(action) {
			var _this3 = this;

			var _state = this.state,
			    list = _state.list,
			    selected = _state.selected;

			_server2.default.removeLayer({
				layer_id: list[selected].id
			}).then(function (_ref2) {
				var data = _ref2.data;


				_this3.setState({
					list: list.filter(function (_, i) {
						return i !== selected;
					})
				});
			}).catch(this.displayServerError);
		}
	}, {
		key: 'handleLayerActionAdd',
		value: function handleLayerActionAdd(action) {
			var _this4 = this;

			_server2.default.addLayer({}).then(function (_ref3) {
				var data = _ref3.data;
				var _state2 = _this4.state,
				    list = _state2.list,
				    selected = _state2.selected,
				    maxId = _state2.maxId;

				var nextId = maxId + 1;
				var layer = Object.assign({ id: nextId }, _config.Config.layerTemplate);
				layer.markers = [];

				var added = void 0;
				if (!list.length) {
					list.push(layer);
					added = 0;
				} else {
					if (list[selected]) {
						added = selected + 1;
					} else {
						added = list.length;
					}
					list.splice(added, 0, layer);
				}
				_this4.setState({
					list: list,
					selected: added,
					edit: action,
					maxId: nextId
				});
			}).catch(this.displayServerError);
		}
	}, {
		key: 'handleLayerActionEdit',
		value: function handleLayerActionEdit(action) {
			this.setState({
				edit: action
			});
		}
	}, {
		key: 'handleLayerActionSave',
		value: function handleLayerActionSave(action) {
			var _state3 = this.state,
			    list = _state3.list,
			    selected = _state3.selected;

			this.updateLayer(list[selected]);
		}
	}, {
		key: 'updateLayer',
		value: function updateLayer(values) {
			var _this5 = this;

			var _state4 = this.state,
			    list = _state4.list,
			    selected = _state4.selected;

			['title', 'attribution', 'url'].forEach(function (key) {
				list[selected][key] = values[key];
			});

			_server2.default.updateLayer({
				layer: list[selected]
			}).then(function (_ref4) {
				var data = _ref4.data;

				_this5.setState({
					list: list,
					edit: ''
				});
			}).catch(this.displayServerError);
		}
	}, {
		key: 'addMarker',
		value: function addMarker(dataObject) {
			var _state5 = this.state,
			    selected = _state5.selected,
			    list = _state5.list;

			if (!list || !list[selected]) return;

			list[selected].markers.push({
				lat: dataObject.latlng.lat,
				lon: dataObject.latlng.lng
			});
			this.setState({
				list: list
			});
		}
	}, {
		key: 'removeMarker',
		value: function removeMarker(index) {
			var _state6 = this.state,
			    selected = _state6.selected,
			    list = _state6.list;

			if (!list || !list[selected]) return;

			list[selected].markers = list[selected].markers.filter(function (_, i) {
				return i !== index;
			});
			this.setState({
				list: list
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this6 = this;

			var _state7 = this.state,
			    error = _state7.error,
			    list = _state7.list,
			    edit = _state7.edit,
			    selected = _state7.selected;


			if (error === 'app') {

				return _react2.default.createElement(_app_error2.default, null);
			} else {

				return _react2.default.createElement(
					_reactBootstrap.Grid,
					{ className: 'map-layers-container', ref: 'module' },
					_react2.default.createElement(
						_reactBootstrap.Row,
						null,
						_react2.default.createElement(
							_reactBootstrap.Col,
							{ md: 8, className: 'map-container' },
							_react2.default.createElement(_map2.default, {
								onClick: this.addMarker,
								onMarkerClick: this.removeMarker,
								layer: list.length && list[selected] ? list[selected] : null
							})
						),
						_react2.default.createElement(
							_reactBootstrap.Col,
							{ md: 4, className: 'layers-select' },
							_react2.default.createElement(_layers_list2.default, {
								list: list,
								selected: this.state.selected,
								onAction: this.handleLayerAction,
								onReload: this.reloadList,
								onSelect: this.setNewLayerOnMap
							})
						)
					),
					_react2.default.createElement(_edit_layer2.default, {
						item: list[selected],
						show: edit !== '',
						onClose: function onClose() {
							_this6.setState({ edit: '' });
						},
						onUpdate: this.updateLayer
					}),
					error === 'server' ? _react2.default.createElement(_server_error2.default, { action: function action() {
							_this6.reloadList();
						} }) : '',
					_react2.default.createElement('div', { id: 'overlay-root' })
				);
			}
		}
	}]);

	return App;
}(_react2.default.Component);

exports.default = App;

/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactAutobind = __webpack_require__(40);

var _reactAutobind2 = _interopRequireDefault(_reactAutobind);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = __webpack_require__(41);

var _icon = __webpack_require__(120);

var _server_error = __webpack_require__(198);

var _server_error2 = _interopRequireDefault(_server_error);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LayersList = function (_React$Component) {
	_inherits(LayersList, _React$Component);

	function LayersList(props) {
		_classCallCheck(this, LayersList);

		var _this = _possibleConstructorReturn(this, (LayersList.__proto__ || Object.getPrototypeOf(LayersList)).call(this, props));

		(0, _reactAutobind2.default)(_this);
		return _this;
	}

	_createClass(LayersList, [{
		key: "layerSelected",
		value: function layerSelected(index) {
			this.props.onSelect(index);
		}
	}, {
		key: "render",
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    list = _props.list,
			    onAction = _props.onAction,
			    onReload = _props.onReload,
			    selected = _props.selected;


			if (list === false) {
				return _react2.default.createElement(_icon.Icon, { name: 'spinner fa-pulse fa-4x' });
			}

			if (list === null) {
				return _react2.default.createElement(_server_error2.default, null);
			}
			return [_react2.default.createElement(
				_reactBootstrap.ListGroup,
				{ key: 'layers-list' },
				list.map(function (layer, index) {
					return _react2.default.createElement(
						_reactBootstrap.ListGroupItem,
						{ className: 'layers-list-item', header: layer.title, key: "layers-list-item-" + index, onClick: function onClick() {
								_this2.layerSelected(index);
							}, active: selected === index },
						_react2.default.createElement(
							"small",
							null,
							"id ",
							layer.id,
							" ",
							_react2.default.createElement(
								"span",
								null,
								"(\u043C\u0430\u0440\u043A\u0435\u0440\u043E\u0432: ",
								layer.markers.length,
								")"
							)
						)
					);
				})
			), _react2.default.createElement(
				_reactBootstrap.Navbar,
				{ key: 'layers-list-controls', className: 'layers-list-controls' },
				_react2.default.createElement(
					_reactBootstrap.Navbar.Form,
					{ pullLeft: true },
					_react2.default.createElement(
						_reactBootstrap.Button,
						{
							bsStyle: 'default',
							onClick: function onClick() {
								onAction('Save');
							},
							disabled: !list.length || !list[selected],
							title: 'Сохранить маркеры',
							style: {
								float: 'left'
							} },
						_react2.default.createElement(_icon.Icon, { name: "save" })
					),
					_react2.default.createElement(
						_reactBootstrap.Button,
						{
							bsStyle: 'default',
							onClick: function onClick() {
								onAction('Add');
							},
							title: 'Добавить' },
						_react2.default.createElement(_icon.Icon, { name: "plus" })
					),
					_react2.default.createElement(
						_reactBootstrap.Button,
						{
							bsStyle: 'default',
							disabled: !list.length || !list[selected],
							onClick: function onClick() {
								onAction('Edit');
							},
							title: 'Изменить' },
						_react2.default.createElement(_icon.Icon, { name: "edit" })
					),
					_react2.default.createElement(
						_reactBootstrap.Button,
						{
							bsStyle: 'default',
							disabled: !list.length || !list[selected],
							onClick: function onClick() {
								onAction('Remove');
							},
							title: 'Удалить' },
						_react2.default.createElement(
							"span",
							{ className: 'text-danger' },
							_react2.default.createElement(_icon.Icon, { name: "trash" })
						)
					),
					_react2.default.createElement(
						_reactBootstrap.Button,
						{
							bsStyle: 'default',
							onClick: onReload,
							title: 'Сбросить',
							style: {
								opacity: 0.5,
								float: 'right'
							} },
						_react2.default.createElement(_icon.Icon, { name: "sync" })
					)
				)
			)];
		}
	}]);

	return LayersList;
}(_react2.default.Component);

exports.default = LayersList;

/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactAutobind = __webpack_require__(40);

var _reactAutobind2 = _interopRequireDefault(_reactAutobind);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(12);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _icon = __webpack_require__(120);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Overlay = function (_React$Component) {
	_inherits(Overlay, _React$Component);

	function Overlay(props) {
		_classCallCheck(this, Overlay);

		var _this = _possibleConstructorReturn(this, (Overlay.__proto__ || Object.getPrototypeOf(Overlay)).call(this, props));

		(0, _reactAutobind2.default)(_this);
		return _this;
	}

	_createClass(Overlay, [{
		key: "render",
		value: function render() {
			var node = document.getElementById('overlay-root');
			if (!node) {
				return '';
			}
			return _reactDom2.default.createPortal(this.props.show ? _react2.default.createElement(
				"div",
				{ className: "overlay" },
				this.props.content ? this.props.content : _react2.default.createElement(_icon.Icon, { name: 'spinner fa-pulse fa-4x' })
			) : _react2.default.createElement("div", null), node);
		}
	}]);

	return Overlay;
}(_react2.default.Component);

exports.default = Overlay;

/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactAutobind = __webpack_require__(40);

var _reactAutobind2 = _interopRequireDefault(_reactAutobind);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = __webpack_require__(41);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EditLayer = function (_React$Component) {
	_inherits(EditLayer, _React$Component);

	function EditLayer(props, context) {
		_classCallCheck(this, EditLayer);

		var _this = _possibleConstructorReturn(this, (EditLayer.__proto__ || Object.getPrototypeOf(EditLayer)).call(this, props, context));

		_this.state = {
			title: '',
			attribution: '',
			url: '',
			id: -1
		};
		(0, _reactAutobind2.default)(_this);
		return _this;
	}

	_createClass(EditLayer, [{
		key: "handleHide",
		value: function handleHide() {
			this.setState({
				id: -1
			});
			this.props.onClose();
		}
	}, {
		key: "handleSave",
		value: function handleSave() {
			this.props.onUpdate(this.state);
			this.handleHide();
		}
	}, {
		key: "handleChange",
		value: function handleChange(e) {
			this.setState(_defineProperty({}, e.target.name, e.target.value));
		}
	}, {
		key: "componentWillReceiveProps",
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.item && this.state.id !== nextProps.item.id) {
				var _nextProps$item = nextProps.item,
				    title = _nextProps$item.title,
				    attribution = _nextProps$item.attribution,
				    url = _nextProps$item.url,
				    id = _nextProps$item.id;

				this.setState({
					title: title,
					attribution: attribution,
					url: url,
					id: id
				});
			}
		}
	}, {
		key: "render",
		value: function render() {
			var _state = this.state,
			    title = _state.title,
			    attribution = _state.attribution,
			    url = _state.url,
			    id = _state.id;


			return _react2.default.createElement(
				_reactBootstrap.Modal,
				{
					show: this.props.show,
					onHide: this.handleHide,
					"aria-labelledby": "contained-modal-title"
				},
				_react2.default.createElement(
					_reactBootstrap.Modal.Header,
					{ closeButton: true },
					_react2.default.createElement(
						_reactBootstrap.Modal.Title,
						{ id: "contained-modal-title" },
						"\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u0441\u043B\u043E\u0439 ",
						_react2.default.createElement(
							"small",
							null,
							"(id: ",
							id,
							")"
						)
					)
				),
				_react2.default.createElement(
					_reactBootstrap.Modal.Body,
					null,
					_react2.default.createElement(
						"form",
						null,
						_react2.default.createElement(
							_reactBootstrap.FormGroup,
							{ controlId: "edit-layer" },
							_react2.default.createElement(
								_reactBootstrap.ControlLabel,
								null,
								"\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 (title)"
							),
							_react2.default.createElement(_reactBootstrap.FormControl, {
								type: "text",
								name: "title",
								value: title,
								onChange: this.handleChange
							}),
							_react2.default.createElement(
								_reactBootstrap.ControlLabel,
								null,
								"\u0410\u0432\u0442\u043E\u0440\u0441\u0442\u0432\u043E (attribution)"
							),
							_react2.default.createElement(_reactBootstrap.FormControl, {
								type: "text",
								name: "attribution",
								value: attribution,
								onChange: this.handleChange
							}),
							_react2.default.createElement(
								_reactBootstrap.ControlLabel,
								null,
								"Url"
							),
							_react2.default.createElement(_reactBootstrap.FormControl, {
								type: "text",
								name: "url",
								value: url,
								onChange: this.handleChange
							}),
							_react2.default.createElement(_reactBootstrap.FormControl.Feedback, null)
						)
					)
				),
				_react2.default.createElement(
					_reactBootstrap.Modal.Footer,
					null,
					_react2.default.createElement(
						_reactBootstrap.Button,
						{ bsStyle: 'primary', onClick: this.handleSave },
						this.props.saveTitle || 'Сохранить'
					),
					_react2.default.createElement(
						_reactBootstrap.Button,
						{ onClick: this.handleHide },
						"\u041E\u0442\u043C\u0435\u043D\u0430"
					)
				)
			);
		}
	}]);

	return EditLayer;
}(_react2.default.Component);

exports.default = EditLayer;

/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _config = __webpack_require__(121);

var _axios = __webpack_require__(199);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Server = function () {
	function Server() {
		_classCallCheck(this, Server);
	}

	_createClass(Server, null, [{
		key: "reloadList",
		value: function reloadList() {
			return (0, _axios2.default)({
				method: 'get',
				url: _config.Config.apiUrl + "layers_list.json",
				responseType: 'json'
			});
		}
	}, {
		key: "updateLayer",
		value: function updateLayer() {
			var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			return (0, _axios2.default)({
				method: 'get', //just simple stub (usually it is post)
				data: data,
				url: _config.Config.apiUrl + "update_layer.json",
				responseType: 'json'
			});
		}
	}, {
		key: "addLayer",
		value: function addLayer() {
			var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			return (0, _axios2.default)({
				method: 'get', //just simple stub (usually it is post)
				data: data,
				url: _config.Config.apiUrl + "add_layer.json",
				responseType: 'json'
			});
		}
	}, {
		key: "removeLayer",
		value: function removeLayer() {
			var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			return (0, _axios2.default)({
				method: 'get', //just simple stub (usually it is post)
				data: data,
				url: _config.Config.apiUrl + "remove_layer.json",
				responseType: 'json'
			});
		}
	}]);

	return Server;
}();

exports.default = Server;

/***/ }),

/***/ 418:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(12);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactAutobind = __webpack_require__(40);

var _reactAutobind2 = _interopRequireDefault(_reactAutobind);

var _reactLeaflet = __webpack_require__(205);

var _config = __webpack_require__(121);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MapArea = function (_React$Component) {
	_inherits(MapArea, _React$Component);

	function MapArea(props) {
		_classCallCheck(this, MapArea);

		var _this = _possibleConstructorReturn(this, (MapArea.__proto__ || Object.getPrototypeOf(MapArea)).call(this, props));

		_this.state = _extends({}, _config.Config.map, {
			layer: null,
			id: null
		});
		(0, _reactAutobind2.default)(_this);
		return _this;
	}

	_createClass(MapArea, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.layer && this.state.id !== nextProps.layer.id) {
				this.setState({
					layer: nextProps.layer,
					id: nextProps.layer.id
				});
			} else if (!nextProps.layer) {
				this.setState({
					layer: null,
					id: null
				});
			}
		}
	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate() {
			var node = document.querySelector('.map-container .leaflet-control-attribution');
			if (node && this.state.layer) {
				node.innerHTML = '<span class="leaflet-control-attribution-custom">' + this.state.layer.attribution + '</span>';
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    mapLat = _state.mapLat,
			    mapLon = _state.mapLon,
			    zoom = _state.zoom,
			    layer = _state.layer;
			var _props = this.props,
			    onClick = _props.onClick,
			    onMarkerClick = _props.onMarkerClick;


			return _react2.default.createElement(
				_reactLeaflet.Map,
				{ center: [mapLat, mapLon], ref: 'map', zoom: zoom, onClick: onClick },
				_react2.default.createElement(_reactLeaflet.TileLayer, {
					attribution: '',
					url: layer ? layer.url : _config.Config.layerTemplate.url
				}),
				(layer ? layer.markers : []).map(function (marker, index) {
					return _react2.default.createElement(_reactLeaflet.Marker, {
						onClick: function onClick() {
							onMarkerClick(index);
						},
						key: 'market-' + layer.id + '-' + index,
						position: [marker.lat, marker.lon] });
				})
			);
		}
	}]);

	return MapArea;
}(_react2.default.Component);

exports.default = MapArea;

/***/ }),

/***/ 555:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = __webpack_require__(41);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AppError = function (_React$Component) {
	_inherits(AppError, _React$Component);

	function AppError(props) {
		_classCallCheck(this, AppError);

		return _possibleConstructorReturn(this, (AppError.__proto__ || Object.getPrototypeOf(AppError)).call(this, props));
	}

	_createClass(AppError, [{
		key: 'render',
		value: function render() {

			return _react2.default.createElement(
				_reactBootstrap.Grid,
				null,
				_react2.default.createElement(
					_reactBootstrap.Row,
					null,
					_react2.default.createElement(
						_reactBootstrap.Col,
						{ md: 12 },
						_react2.default.createElement(
							_reactBootstrap.Alert,
							{ bsStyle: 'danger', onDismiss: this.props.action },
							_react2.default.createElement(
								'h4',
								null,
								'\u041E\u0448\u0438\u0431\u043A\u0430 \u0432 \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u0438 :('
							),
							_react2.default.createElement(
								'p',
								null,
								'\u0412\u0441\u0435 \u0434\u0435\u0442\u0430\u043B\u0438 \u0432 \u043A\u043E\u043D\u0441\u043E\u043B\u0438...'
							)
						)
					)
				)
			);
		}
	}]);

	return AppError;
}(_react2.default.Component);

exports.default = AppError;

/***/ })

},[235]);
//# sourceMappingURL=index.js.map
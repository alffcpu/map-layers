export const Config = {
	apiUrl: 'dist/api/',
	layerTemplate: {
		title: "New Map Layer",
		attribution: "",
		url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
	},
	map: {
		mapLat: 48.8,
		mapLon: 132.95,
		zoom: 13
	}
};

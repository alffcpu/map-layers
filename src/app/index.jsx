import React from 'react';
import ReactDOM from 'react-dom';
import autoBind from 'react-autobind';
import { Grid, Col, Row } from 'react-bootstrap';
import LayersList from 'componentsPath/layers_list';
import EditLayer from 'componentsPath/edit_layer';
import Server from 'utilsPath/server.js';
import {Config} from "configPath/config";
import MapArea from 'componentsPath/map';
import ServerError from 'componentsPath/server_error';
import AppError from 'componentsPath/app_error';

class App extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			error: '',
			list: false,
			selected: 0,
			edit: '',
			maxId: 0
		};
		autoBind(this);
	}

	componentDidCatch() {
		this.setState({
			error: 'app'
		});
	}

	componentDidMount() {
		this.reloadList();
	}

	reloadList() {
		Server.reloadList().then(({data}) => {
			let maxId = 0;
			data.layers.forEach((row) => {
				if (row.id > maxId) maxId = row.id;
			});
			this.setState({
				maxId: maxId++,
				list: data.layers,
				selected: 0,
				edit: ''
			});
		})
		.catch(this.displayServerError);
	}

	displayServerError() {
		this.setState({
			list: null,
			error: 'server'
		});
	}

	setNewLayerOnMap(index) {
		this.setState({
			selected: index
		});
	}

	handleLayerAction(action) {
		const method = `handleLayerAction${action}`;
		if (typeof this[method] !== 'undefined') {
			this[method](action);
		}
	}

	handleLayerActionRemove(action) {
		const {list, selected} = this.state;
		Server.removeLayer({
			layer_id: list[selected].id
		}).then(({data}) => {

			this.setState({
				list: list.filter((_, i) => i !== selected)
			});

		}).catch(this.displayServerError);
	}

	handleLayerActionAdd(action) {
		Server.addLayer({

		}).then(({data}) => {

			const {list, selected, maxId} = this.state;
			const nextId = maxId + 1;
			const layer = Object.assign({id: nextId}, Config.layerTemplate);
			layer.markers = [];

			let added;
			if (!list.length) {
				list.push(layer);
				added = 0;
			} else {
				if (list[selected]) {
					added = selected + 1
				} else {
					added = list.length;
				}
				list.splice(added, 0, layer);
			}
			this.setState({
				list: list,
				selected: added,
				edit: action,
				maxId: nextId
			});

		}).catch(this.displayServerError);
	}

	handleLayerActionEdit(action) {
		this.setState({
			edit: action
		});
	}

	handleLayerActionSave(action) {
		const { list, selected } = this.state;
		this.updateLayer(list[selected]);
	}

	updateLayer(values) {
		const { list, selected } = this.state;
		['title', 'attribution', 'url'].forEach((key) => {
			list[selected][key] = values[key];
		});

		Server.updateLayer({
			layer: list[selected]
		}).then(({data}) => {
			this.setState({
				list: list,
				edit: ''
			});
		}).catch(this.displayServerError);
	}

	addMarker(dataObject) {
		const {selected, list} = this.state;
		if (!list || !list[selected]) return;

		list[selected].markers.push({
			lat: dataObject.latlng.lat,
			lon: dataObject.latlng.lng
		});
		this.setState({
			list: list
		})
	}

	removeMarker(index) {
		const {selected, list} = this.state;
		if (!list || !list[selected]) return;

		list[selected].markers = list[selected].markers.filter( (_, i) => i !== index );
		this.setState({
			list: list
		})
	}

	render() {

		const { error, list, edit, selected} = this.state;

		if ( error === 'app') {

			return <AppError />

		} else {

			return <Grid className="map-layers-container" ref="module">
					<Row>
						<Col md={8} className="map-container">
							<MapArea
								onClick={this.addMarker}
								onMarkerClick={this.removeMarker}
								layer={ (list.length && list[selected]) ? list[selected] : null }
							/>
						</Col>
						<Col md={4} className="layers-select">
							<LayersList
								list={list}
								selected={this.state.selected}
								onAction={ this.handleLayerAction }
								onReload={this.reloadList}
								onSelect={this.setNewLayerOnMap}
							/>
						</Col>
					</Row>
					<EditLayer
						item={ list[selected] }
						show={ edit !== '' }
						onClose={ () => { this.setState({edit: ''}) } }
						onUpdate={ this.updateLayer }
					/>
					{ error === 'server' ? <ServerError action={ () => { this.reloadList() } } />: '' }
					<div id="overlay-root" />
				</Grid>;
		}
	}
}
export default App;
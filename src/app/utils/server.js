import {Config} from "configPath/config";
import axios from "axios";

class Server {

	static reloadList() {
		return axios({
			method: 'get',
			url:`${Config.apiUrl}layers_list.json`,
			responseType: 'json'
		})
	}

	static updateLayer(data = {}) {
		return axios({
			method: 'get', //just simple stub (usually it is post)
			data: data,
			url:`${Config.apiUrl}update_layer.json`,
			responseType: 'json'
		})
	}

	static addLayer(data = {}) {
		return axios({
			method: 'get', //just simple stub (usually it is post)
			data: data,
			url:`${Config.apiUrl}add_layer.json`,
			responseType: 'json'
		})
	}

	static removeLayer(data = {}) {
		return axios({
			method: 'get', //just simple stub (usually it is post)
			data: data,
			url:`${Config.apiUrl}remove_layer.json`,
			responseType: 'json'
		})
	}

}

export default Server;
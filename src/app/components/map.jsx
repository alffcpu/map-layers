import React from 'react';
import ReactDOM from 'react-dom';
import autoBind from 'react-autobind';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { Config } from "configPath/config";

class MapArea extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			...Config.map,
			layer: null,
			id: null
		};
		autoBind(this);
	}

	componentWillReceiveProps(nextProps) {
		if ( nextProps.layer && (this.state.id !== nextProps.layer.id ) ) {
			this.setState({
				layer: nextProps.layer,
				id: nextProps.layer.id
			});
		} else if (!nextProps.layer) {
			this.setState({
				layer: null,
				id: null
			});
		}
	}

	componentDidUpdate() {
		const node = document.querySelector('.map-container .leaflet-control-attribution');
		if (node && this.state.layer) {
			node.innerHTML = `<span class="leaflet-control-attribution-custom">${this.state.layer.attribution}</span>`;
		}
	}

	render() {
		const { mapLat, mapLon, zoom, layer } = this.state;
		const { onClick, onMarkerClick } = this.props;

		return <Map center={ [mapLat, mapLon] } ref="map" zoom={zoom} onClick={onClick} >
			<TileLayer
				attribution={ '' }
				url={ layer ? layer.url : Config.layerTemplate.url }
			/>
			{( layer ? layer.markers : []).map((marker, index) => {
				return <Marker
							onClick={ () => { onMarkerClick(index) } }
							key={`market-${layer.id}-${index}`}
							position={[marker.lat, marker.lon]} />
			})}
		</Map>
	}
}

export default MapArea;
import autoBind from "react-autobind";
import React from "react";
import { ListGroup, ListGroupItem, Button, Alert, Navbar } from 'react-bootstrap';
import { Icon } from 'componentsPath/icon';
import ServerError from 'componentsPath/server_error';

class LayersList extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	layerSelected(index) {
		this.props.onSelect(index);
	}

	render() {

		const {list, onAction, onReload, selected} = this.props;

		if (list === false) {
			return <Icon name={'spinner fa-pulse fa-4x'} />
		}

		if (list === null) {
			return <ServerError />
		}
		return [
			<ListGroup key={'layers-list'}>
				{list.map((layer, index) => {
						return <ListGroupItem className={'layers-list-item'} header={layer.title} key={`layers-list-item-${index}`} onClick={ () => {this.layerSelected(index)} } active={ selected === index }>
							<small>id {layer.id} <span>(маркеров: {layer.markers.length})</span></small>
						</ListGroupItem>
					}
				)}
			</ListGroup>,
			<Navbar key={'layers-list-controls'} className={'layers-list-controls'}>
				<Navbar.Form pullLeft>
					<Button
						bsStyle={'default'}
						onClick={ () => { onAction('Save') } }
						disabled={ !list.length || !list[selected] }
						title={'Сохранить маркеры'}
						style={{
							float: 'left'
						}}><Icon name="save" /></Button>
					<Button
						bsStyle={'default'}
						onClick={ () => { onAction('Add') } }
						title={'Добавить'}><Icon name="plus" /></Button>
					<Button
						bsStyle={'default'}
						disabled={ !list.length || !list[selected] }
						onClick={ () => { onAction('Edit') } }
						title={'Изменить'}><Icon name="edit" /></Button>
					<Button
						bsStyle={'default'}
						disabled={ !list.length || !list[selected] }
						onClick={ () => { onAction('Remove') } }
						title={'Удалить'}><span className={'text-danger'}><Icon name="trash" /></span></Button>
					<Button
						bsStyle={'default'}
						onClick={ onReload }
						title={'Сбросить'}
						style={{
							opacity: 0.5,
							float: 'right'
						}}><Icon name="sync" /></Button>
				</Navbar.Form>
			</Navbar>
		]
	}
}
export default LayersList;
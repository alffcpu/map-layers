import autoBind from "react-autobind";
import React from "react";
import ReactDOM from "react-dom";
import { Icon } from 'componentsPath/icon';

class Overlay extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {
		const node = document.getElementById('overlay-root');
		if (!node) {
			return '';
		}
		return ReactDOM.createPortal(
			(
				this.props.show ?
				<div className="overlay">{ this.props.content ? this.props.content : <Icon name={'spinner fa-pulse fa-4x'} /> }</div> :
				<div />
			),
			node
		);
	}
}

export default Overlay;
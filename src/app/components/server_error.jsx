import {Icon} from 'componentsPath/icon';
import React from 'react';
import { Alert, Button } from 'react-bootstrap';
import Overlay from 'componentsPath/overlay';

class ServerError extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		const alert = <Alert bsStyle="danger" className={'alert-inboxed'} onDismiss={this.props.action}>
			<h4>Ошибка взаимодействия с сервером!</h4>
			<p>
				Не удалось прочитать/сохранить данные...
			</p>
			<p>
				<Button bsStyle="danger" onClick={this.props.action}>{ this.props.actionText || 'Ок' }</Button>
			</p>
		</Alert>;

		return <Overlay show={true} content={alert} />;
	}
}

export default ServerError;
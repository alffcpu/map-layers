import React from 'react';
import { Alert, Grid, Col, Row } from 'react-bootstrap';
class AppError extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		return <Grid>
			<Row>
				<Col md={12}>
					<Alert bsStyle="danger" onDismiss={this.props.action}>
						<h4>Ошибка в приложении :(</h4>
						<p>
							Все детали в консоли...
						</p>
					</Alert>
				</Col>
			</Row>
		</Grid>;
	}
}

export default AppError;
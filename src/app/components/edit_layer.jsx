import autoBind from "react-autobind";
import React from "react";
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock, FieldGroup } from 'react-bootstrap';

class EditLayer extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			title: '',
			attribution: '',
			url: '',
			id: -1
		};
		autoBind(this);
	}

	handleHide() {
		this.setState({
			id: -1
		});
		this.props.onClose();
	}

	handleSave() {
		this.props.onUpdate(this.state);
		this.handleHide();
	}

	handleChange(e) {
		this.setState({[e.target.name]: e.target.value});
	}

	componentWillReceiveProps(nextProps) {
		if ( nextProps.item && (this.state.id !== nextProps.item.id) ) {
			const {title, attribution, url, id} = nextProps.item;
			this.setState({
				title,
				attribution,
				url,
				id
			});
		}
	}

	render() {
		const { title, attribution, url, id } = this.state;

		return (
			<Modal
				show={this.props.show}
				onHide={this.handleHide}
				aria-labelledby="contained-modal-title"
			>
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title">
						Изменить слой <small>(id: {id})</small>
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<form>
						<FormGroup controlId="edit-layer">
							<ControlLabel>Название (title)</ControlLabel>
							<FormControl
								type="text"
								name="title"
								value={title}
								onChange={this.handleChange}
							/>
							<ControlLabel>Авторство (attribution)</ControlLabel>
							<FormControl
								type="text"
								name="attribution"
								value={attribution}
								onChange={this.handleChange}
							/>
							<ControlLabel>Url</ControlLabel>
							<FormControl
								type="text"
								name="url"
								value={url}
								onChange={this.handleChange}
							/>
							<FormControl.Feedback />
						</FormGroup>
					</form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={'primary'} onClick={this.handleSave}>{this.props.saveTitle || 'Сохранить'}</Button>
					<Button onClick={this.handleHide}>Отмена</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}

export default EditLayer;